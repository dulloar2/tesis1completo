﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Sql;

namespace TesisCompleto
{
    public partial class Menu : Form
    {
        //exepto el menu lateral todo lo demas por defoult no son visibles
        public Menu()
        {
            InitializeComponent();
        }
        private void BotonComprasVnetas_Click(object sender, EventArgs e)
        {

        }

        private void Menu_Load(object sender, EventArgs e)
        {


            dataGridView2TrabajadorasClientes.Visible = false;
            Facturaono.Visible = false;
            checkboxTieneTrabajadores.Visible = false;
            groupBoxTienesAdeudos.Visible = false;
            groupBox1.Visible = false;
            GroupBoxManejaProductosPereceders.Visible = false;
        }

        private void comprasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GuadarDatosConCaducidad.Visible = false;
            ModificarVenta.Visible = false;
            BorrardeVneta.Visible = false;
            AgregarVenta.Visible = false;
            dataGridView1conCaducidad.Visible = false;
            Busqueda.Visible = false;
            GroupBoxManejaProductosPereceders.Visible = false;
            BotonAgregarProducto.Visible = false;
            BotonDardeBajaProducto.Visible = false;
            BotonGuardarCambios.Visible = false;
            groupBoxDarDeBajaEsteProducto.Visible = false;
            dataGridView2Modificardatos.Visible = false;
            BotonFinalizarventaConFactura.Visible = false;
            BotonFinalizarVentaConNota.Visible = false;
            BotonFinalizarVentaSimple.Visible = false;
            dataGridView2ParalaVenta.Visible = false;
            dataGridView2Ventassimples.Visible = false;
            EtiquetadeAdbertenciadeSeleccion.Visible = false;
            Facturaono.Visible = false;
            checkboxTieneTrabajadores.Visible = false;
            groupBoxTienesAdeudos.Visible = false;
            groupBox1.Visible = false;
            dataGridView2TrabajadorasClientes.Visible = false;
            dataGridView1.Visible = false;
        }

        private void BotonEmpresa_Click(object sender, EventArgs e)
        {

        }

        private void productosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            GuadarDatosConCaducidad.Visible = true;
            GuadarDatosConCaducidad.Location = new Point(600,650);
            ModificarVenta.Visible = false;
            BorrardeVneta.Visible = false;
            AgregarVenta.Visible = false;
            dataGridView1conCaducidad.Visible = true;
            dataGridView1conCaducidad.Location = new Point(200,400);
            dataGridView1conCaducidad.Size = new System.Drawing.Size(1045, 150);
            Busqueda.Visible = false;
            GroupBoxManejaProductosPereceders.Visible = false;
            BotonAgregarProducto.Visible = false;
            BotonDardeBajaProducto.Visible = false;
            BotonGuardarCambios.Visible = false;
            groupBoxDarDeBajaEsteProducto.Visible = false;
            dataGridView2Modificardatos.Visible = false;
            BotonFinalizarventaConFactura.Visible = false;
            BotonFinalizarVentaConNota.Visible = false;
            BotonFinalizarVentaSimple.Visible = false;
            dataGridView2ParalaVenta.Visible = false;
            dataGridView2Ventassimples.Visible = false;
            BotonGuardardatostrabajadoresclientes.Visible = false;
            BotonGuardarDatosGenerales.Visible = false;
            dataGridView1.Visible = true;                          //este daragridview1 es donde se llenaran los productos y aqui se hace visible
            dataGridView1.Location = new Point(200, 200);
            BotonGuardarProductos.Visible = true;                  //Este boton esta aqui para guardar los datos 
            BotonGuardarProductos.Location = new Point(1000, 650);
            dataGridView1.Size = new System.Drawing.Size(1045, 150);//Esta parte controla el tamaño de el data grid view
            EtiquetadeAdbertenciadeSeleccion.Visible = false;
            Facturaono.Visible = false;
            checkboxTieneTrabajadores.Visible = false;
            groupBoxTienesAdeudos.Visible = false;
            groupBox1.Visible = false;
            dataGridView2TrabajadorasClientes.Visible = false;                            //Se pone visible el data grid view Trabajadores clientes 
        }

        private void clientesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            GuadarDatosConCaducidad.Visible = false;
            ModificarVenta.Visible = false;
            BorrardeVneta.Visible = false;
            AgregarVenta.Visible = false;
            dataGridView1conCaducidad.Visible = false;
            Busqueda.Visible = false;
            GroupBoxManejaProductosPereceders.Visible = false;
            BotonAgregarProducto.Visible = false;
            BotonDardeBajaProducto.Visible = false;
            BotonGuardarCambios.Visible = false;
            groupBoxDarDeBajaEsteProducto.Visible = false;
            dataGridView2Modificardatos.Visible = false;
            BotonFinalizarventaConFactura.Visible = false;
            BotonFinalizarVentaConNota.Visible = false;
            BotonFinalizarVentaSimple.Visible = false;
            dataGridView2ParalaVenta.Visible = false;
            dataGridView2Ventassimples.Visible = false;
            BotonGuardardatostrabajadoresclientes.Location = new Point(900, 615);
            BotonGuardarDatosGenerales.Visible = false;
            dataGridView1.Visible = false;                                               //Se pone invisible el data grid view para llenar Productos
            BotonGuardarProductos.Visible = false;                                       //Se pone invisible el Boton de guardar producto
            dataGridView2TrabajadorasClientes.Location = new Point(80, 200);             //Se colocara el data grid view Trabajadores clientes por que no esta en el mismo lugar que el otro pero si tiene las mismas dimencuiones originales
            dataGridView2TrabajadorasClientes.Visible = true;                            //Se pone visible el data grid view Trabajadores clientes 
            dataGridView2TrabajadorasClientes.Size = new System.Drawing.Size(1243, 150);   //Se le dan las dimenciones a el data grid view Trabajadores Clientes.
            BotonGuardardatostrabajadoresclientes.Visible = true;
            Facturaono.Visible = true;
            EtiquetadeAdbertenciadeSeleccion.Visible = false;
            Facturaono.Visible = false;
            checkboxTieneTrabajadores.Visible = false;
            groupBoxTienesAdeudos.Visible = false;
            groupBox1.Visible = false;


        }

        private void datosBasicosToolStripMenuItem_Click(object sender, EventArgs e)
        {

            GuadarDatosConCaducidad.Visible = false;
            ModificarVenta.Visible = false;
            BorrardeVneta.Visible = false;
            AgregarVenta.Visible = false;
            dataGridView1conCaducidad.Visible = false;
            Busqueda.Visible = false;
            GroupBoxManejaProductosPereceders.Visible = true;
            GroupBoxManejaProductosPereceders.Location = new Point(1100, 300);
            BotonDardeBajaProducto.Visible = false;
            BotonGuardarCambios.Visible = false;
            groupBoxDarDeBajaEsteProducto.Visible = false;
            dataGridView2Modificardatos.Visible = false;
            BotonFinalizarventaConFactura.Visible = false;
            BotonFinalizarVentaConNota.Visible = false;
            BotonFinalizarVentaSimple.Visible = false;
            dataGridView2ParalaVenta.Visible = false;
            dataGridView2Ventassimples.Visible = false;
            Facturaono.Location = new Point(50, 300);
            checkboxTieneTrabajadores.Location = new Point(200, 300);
            groupBoxTienesAdeudos.Location = new Point(400, 300);
            //groupBox1.Location = new Point(850, 100);
            BotonGuardarDatosGenerales.Location = new Point(550, 600);
            BotonGuardarDatosGenerales.Visible = true;
            dataGridView1.Visible = false;                                               //Se pone invisible el data grid view para llenar Productos
            BotonGuardarProductos.Visible = false;                                       //Se pone invisible el Boton de guardar producto
            dataGridView2TrabajadorasClientes.Visible = false;                            //Se pone visible el data grid view Trabajadores clientes 
            BotonGuardardatostrabajadoresclientes.Visible = false;
            Facturaono.Visible = false;
            EtiquetadeAdbertenciadeSeleccion.Visible = true;
            EtiquetadeAdbertenciadeSeleccion.Location = new Point(170, 200);
            Facturaono.Visible = true;
            checkboxTieneTrabajadores.Visible = true;
            groupBoxTienesAdeudos.Visible = true;
            groupBox1.Visible = true;
            groupBox1.Location = new Point(750, 300);

        }

        private void ventasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GuadarDatosConCaducidad.Visible = false;
            ModificarVenta.Visible = true;
            ModificarVenta.Location = new Point(1100, 300);
            BorrardeVneta.Visible = true;
            BorrardeVneta.Location = new Point(1100, 250);
            AgregarVenta.Visible = true;
            AgregarVenta.Location = new Point(1100, 200);
            dataGridView1conCaducidad.Visible = false;
            Busqueda.Visible = true;
            Busqueda.Location = new Point(20, 300);
            GroupBoxManejaProductosPereceders.Visible = false;
            BotonAgregarProducto.Visible = false;
            BotonDardeBajaProducto.Visible = false;
            BotonGuardarCambios.Visible = false;
            BotonFinalizarVentaConNota.Visible = true;
            BotonFinalizarVentaConNota.Location = new Point(1100, 550);
            groupBoxDarDeBajaEsteProducto.Visible = false;
            dataGridView2Modificardatos.Visible = false;
            BotonFinalizarventaConFactura.Visible = true;
            BotonFinalizarventaConFactura.Location = new Point(1100,450);
            BotonFinalizarVentaSimple.Visible = true;
            BotonFinalizarVentaSimple.Location = new Point(1100, 650);
            dataGridView2ParalaVenta.Visible = false;  //temporal
            dataGridView2Ventassimples.Visible = true;
            dataGridView2Ventassimples.Location = new Point(400, 200);
            dataGridView2Ventassimples.Size = new System.Drawing.Size(645, 450);
            dataGridView2ParalaVenta.Location = new Point(400, 200);
            dataGridView2ParalaVenta.Size = new System.Drawing.Size(645, 450);
            dataGridView2TrabajadorasClientes.Visible = false;
            dataGridView1.Visible = false;
            BotonGuardardatostrabajadoresclientes.Visible = false;
            BotonGuardarProductos.Visible = false;
            BotonGuardarDatosGenerales.Visible = false;
            EtiquetadeAdbertenciadeSeleccion.Visible = false;
            Facturaono.Visible = false;
            checkboxTieneTrabajadores.Visible = false;
            groupBoxTienesAdeudos.Visible = false;
            groupBox1.Visible = false;
        }

        private void ventaSimpleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GuadarDatosConCaducidad.Visible = false;
            ModificarVenta.Visible = true;
            ModificarVenta.Location = new Point(1100, 300);
            BorrardeVneta.Visible = true;
            BorrardeVneta.Location = new Point(1100, 250);
            AgregarVenta.Visible = true;
            AgregarVenta.Location = new Point(1100, 200);
            dataGridView1conCaducidad.Visible = false;
            Busqueda.Visible = true;
            Busqueda.Location = new Point(20, 185);
            GroupBoxManejaProductosPereceders.Visible = false;
            BotonAgregarProducto.Visible = false;
            BotonDardeBajaProducto.Visible = false;
            BotonGuardarCambios.Visible = false;
            groupBoxDarDeBajaEsteProducto.Visible = false;
            dataGridView2Modificardatos.Visible = false;
            BotonFinalizarventaConFactura.Visible = false;
            BotonFinalizarVentaSimple.Visible = true;
            BotonFinalizarVentaSimple.Location = new Point(1100, 650);
            dataGridView2ParalaVenta.Visible = false;  //temporal
            dataGridView2Ventassimples.Visible = true;
            dataGridView2Ventassimples.Location = new Point(400, 200);
            dataGridView2Ventassimples.Size = new System.Drawing.Size(645, 150);
            dataGridView2ParalaVenta.Location = new Point(400, 200);
            dataGridView2ParalaVenta.Size = new System.Drawing.Size(645, 150);
            dataGridView2TrabajadorasClientes.Visible = false;
            dataGridView1.Visible = false;
            BotonGuardardatostrabajadoresclientes.Visible = false;
            BotonGuardarProductos.Visible = false;
            BotonGuardarDatosGenerales.Visible = false;
            EtiquetadeAdbertenciadeSeleccion.Visible = false;
            Facturaono.Visible = false;
            checkboxTieneTrabajadores.Visible = false;
            groupBoxTienesAdeudos.Visible = false;
            groupBox1.Visible = false;
        }

        private void ventaConNotaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GuadarDatosConCaducidad.Visible = false;
            ModificarVenta.Visible = true;
            ModificarVenta.Location = new Point(1100, 300);
            BorrardeVneta.Visible = true;
            BorrardeVneta.Location = new Point(1100, 250);
            AgregarVenta.Visible = true;
            AgregarVenta.Location = new Point(1100, 200);
            dataGridView1conCaducidad.Visible = false;
            Busqueda.Visible = true;
            Busqueda.Location = new Point(60, 200);
            GroupBoxManejaProductosPereceders.Visible = false;
            BotonAgregarProducto.Visible = false;
            BotonDardeBajaProducto.Visible = false;
            BotonGuardarCambios.Visible = false;
            groupBoxDarDeBajaEsteProducto.Visible = false;
            dataGridView2Modificardatos.Visible = false;
            BotonFinalizarventaConFactura.Visible = false;
            BotonFinalizarVentaConNota.Visible = true;
            BotonFinalizarVentaConNota.Location = new Point(1100, 650);
            BotonFinalizarVentaSimple.Visible = false;
            dataGridView2ParalaVenta.Visible = false; //temporal
            dataGridView2Ventassimples.Visible = true;
            dataGridView2Ventassimples.Location = new Point(400, 200);
            dataGridView2Ventassimples.Size = new System.Drawing.Size(645, 150);
            dataGridView2ParalaVenta.Location = new Point(700, 15);
            dataGridView2ParalaVenta.Size = new System.Drawing.Size(400, 150);
            dataGridView2TrabajadorasClientes.Visible = false;
            dataGridView1.Visible = false;
            BotonGuardardatostrabajadoresclientes.Visible = false;
            BotonGuardarProductos.Visible = false;
            BotonGuardarDatosGenerales.Visible = false;
            EtiquetadeAdbertenciadeSeleccion.Visible = false;
            Facturaono.Visible = false;
            checkboxTieneTrabajadores.Visible = false;
            groupBoxTienesAdeudos.Visible = false;
            groupBox1.Visible = false;
        }

        private void ventaConFacturaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GuadarDatosConCaducidad.Visible = false;
            ModificarVenta.Visible = true;
            ModificarVenta.Location = new Point(1100, 300);
            BorrardeVneta.Visible = true;
            BorrardeVneta.Location = new Point(1100, 250);
            AgregarVenta.Visible = true;
            AgregarVenta.Location = new Point(1100, 200);
            dataGridView1conCaducidad.Visible = false;
            Busqueda.Visible = true;
            Busqueda.Location = new Point(60, 200);
            GroupBoxManejaProductosPereceders.Visible = false;
            BotonAgregarProducto.Visible = false;
            BotonDardeBajaProducto.Visible = false;
            BotonGuardarCambios.Visible = false;
            groupBoxDarDeBajaEsteProducto.Visible = false;
            dataGridView2Modificardatos.Visible = false;
            BotonFinalizarventaConFactura.Visible = true;
            BotonFinalizarventaConFactura.Location = new Point(1100, 650);
            BotonFinalizarVentaConNota.Visible = false;
            BotonFinalizarVentaSimple.Visible = false;
            dataGridView2ParalaVenta.Visible = false;  //temporal
            dataGridView2Ventassimples.Visible = true;
            dataGridView2Ventassimples.Location = new Point(400, 200);
            dataGridView2Ventassimples.Size = new System.Drawing.Size(645, 150);
            dataGridView2ParalaVenta.Location = new Point(700, 15);
            dataGridView2ParalaVenta.Size = new System.Drawing.Size(400, 150);
            dataGridView2TrabajadorasClientes.Visible = false;
            dataGridView1.Visible = false;
            BotonGuardardatostrabajadoresclientes.Visible = false;
            BotonGuardarProductos.Visible = false;
            BotonGuardarDatosGenerales.Visible = false;
            EtiquetadeAdbertenciadeSeleccion.Visible = false;
            Facturaono.Visible = false;
            checkboxTieneTrabajadores.Visible = false;
            groupBoxTienesAdeudos.Visible = false;
            groupBox1.Visible = false;
        }

        private void modificarProductoExistenteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GuadarDatosConCaducidad.Visible = false;
            ModificarVenta.Visible = false;
            BorrardeVneta.Visible = false;
            AgregarVenta.Visible = false;
            dataGridView1conCaducidad.Visible = false;
            Busqueda.Visible = false;
            GroupBoxManejaProductosPereceders.Visible = false;
            BotonAgregarProducto.Visible = false;
            BotonDardeBajaProducto.Visible = false;
            BotonGuardarCambios.Visible = true;
            BotonGuardarCambios.Location = new Point(900, 213);
            groupBoxDarDeBajaEsteProducto.Visible = false;
            dataGridView2Modificardatos.Visible = true;
            dataGridView2Modificardatos.Location = new Point(193, 15);
            dataGridView2Modificardatos.Size = new System.Drawing.Size(900, 150);
            BotonFinalizarventaConFactura.Visible = false;
            BotonFinalizarVentaConNota.Visible = false;
            BotonFinalizarVentaSimple.Visible = false;
            dataGridView2ParalaVenta.Visible = false;
            dataGridView2Ventassimples.Visible = false;
            dataGridView2TrabajadorasClientes.Visible = false;
            dataGridView1.Visible = false;
            BotonGuardardatostrabajadoresclientes.Visible = false;
            BotonGuardarProductos.Visible = false;
            BotonGuardarDatosGenerales.Visible = false;
            EtiquetadeAdbertenciadeSeleccion.Visible = false;
            Facturaono.Visible = false;
            checkboxTieneTrabajadores.Visible = false;
            groupBoxTienesAdeudos.Visible = false;
            groupBox1.Visible = false;
        }

        private void darDeBajaUnProductoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GuadarDatosConCaducidad.Visible = false;
            ModificarVenta.Visible = false;
            BorrardeVneta.Visible = false;
            AgregarVenta.Visible = false;
            dataGridView1conCaducidad.Visible = false;
            Busqueda.Visible = false;
            GroupBoxManejaProductosPereceders.Visible = false;
            BotonAgregarProducto.Visible = false;
            BotonDardeBajaProducto.Visible = true;
            BotonDardeBajaProducto.Location = new Point(900, 213);
            BotonGuardarCambios.Visible = false;
            groupBoxDarDeBajaEsteProducto.Location = new Point(400, 200);
            groupBoxDarDeBajaEsteProducto.Visible = true;
            dataGridView2Modificardatos.Visible = true;
            dataGridView2Modificardatos.Location = new Point(193, 15);
            dataGridView2Modificardatos.Size = new System.Drawing.Size(900, 150);
            BotonFinalizarventaConFactura.Visible = false;
            BotonFinalizarVentaConNota.Visible = false;
            BotonFinalizarVentaSimple.Visible = false;
            dataGridView2ParalaVenta.Visible = false;
            dataGridView2Ventassimples.Visible = false;
            dataGridView1.Visible = false;
            BotonGuardardatostrabajadoresclientes.Visible = false;
            BotonGuardarProductos.Visible = false;
            BotonGuardarDatosGenerales.Visible = false;
            EtiquetadeAdbertenciadeSeleccion.Visible = false;
            Facturaono.Visible = false;
            checkboxTieneTrabajadores.Visible = false;
            groupBoxTienesAdeudos.Visible = false;
            groupBox1.Visible = false;
        }

        private void agregarNuevoProductoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GuadarDatosConCaducidad.Visible = false;
            ModificarVenta.Visible = false;
            BorrardeVneta.Visible = false;
            AgregarVenta.Visible = false;
            dataGridView1conCaducidad.Visible = false;
            Busqueda.Visible = false;
            GroupBoxManejaProductosPereceders.Visible = false;
            BotonAgregarProducto.Visible = true;
            BotonDardeBajaProducto.Visible = false;
            BotonGuardarCambios.Visible = false;
            groupBoxDarDeBajaEsteProducto.Visible = false;
            dataGridView2Modificardatos.Visible = true;
            dataGridView2Modificardatos.Location = new Point(193, 15);
            dataGridView2Modificardatos.Size = new System.Drawing.Size(900, 150);
            BotonFinalizarventaConFactura.Visible = false;
            BotonFinalizarVentaConNota.Visible = false;
            BotonFinalizarVentaSimple.Visible = false;
            dataGridView2ParalaVenta.Visible = false;
            dataGridView2Ventassimples.Visible = false;
            dataGridView2TrabajadorasClientes.Visible = false;
            dataGridView1.Visible = false;
            BotonGuardardatostrabajadoresclientes.Visible = false;
            BotonGuardarProductos.Visible = false;
            BotonGuardarDatosGenerales.Visible = false;
            EtiquetadeAdbertenciadeSeleccion.Visible = false;
            Facturaono.Visible = false;
            checkboxTieneTrabajadores.Visible = false;
            groupBoxTienesAdeudos.Visible = false;
            groupBox1.Visible = false;
        }

        private void empresaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GuadarDatosConCaducidad.Visible = false;
            ModificarVenta.Visible = false;
            BorrardeVneta.Visible = false;
            AgregarVenta.Visible = false;
            dataGridView1conCaducidad.Visible = false;
            Busqueda.Visible = false;
            GroupBoxManejaProductosPereceders.Visible = false;
            BotonAgregarProducto.Visible = false;
            BotonDardeBajaProducto.Visible = false;
            BotonGuardarCambios.Visible = false;
            groupBoxDarDeBajaEsteProducto.Visible = false;
            dataGridView2Modificardatos.Visible = false;
            BotonFinalizarventaConFactura.Visible = false;
            BotonFinalizarVentaConNota.Visible = false;
            BotonFinalizarVentaSimple.Visible = false;
            dataGridView2ParalaVenta.Visible = false;
            dataGridView2Ventassimples.Visible = false;
            dataGridView2TrabajadorasClientes.Visible = false;
            dataGridView1.Visible = false;
            BotonGuardardatostrabajadoresclientes.Visible = false;
            BotonGuardarProductos.Visible = false;
            BotonGuardarDatosGenerales.Visible = false;
            BotonGuardarDatosGenerales.Visible = false;
            EtiquetadeAdbertenciadeSeleccion.Visible = false;
            Facturaono.Visible = false;
            checkboxTieneTrabajadores.Visible = false;
            groupBoxTienesAdeudos.Visible = false;
            groupBox1.Visible = false;
        }

        private void llenadoDeDatosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GuadarDatosConCaducidad.Visible = false;
            ModificarVenta.Visible = false;
            BorrardeVneta.Visible = false;
            AgregarVenta.Visible = false;
            dataGridView1conCaducidad.Visible = false;
            Busqueda.Visible = false;
            GroupBoxManejaProductosPereceders.Visible = false;
            BotonAgregarProducto.Visible = false;
            BotonDardeBajaProducto.Visible = false;
            BotonGuardarCambios.Visible = false;
            groupBoxDarDeBajaEsteProducto.Visible = false;
            dataGridView2Modificardatos.Visible = false;
            BotonFinalizarventaConFactura.Visible = false;
            BotonFinalizarVentaConNota.Visible = false;
            BotonFinalizarVentaSimple.Visible = false;
            dataGridView2ParalaVenta.Visible = false;
            dataGridView2Ventassimples.Visible = false;
            dataGridView2TrabajadorasClientes.Visible = false;
            dataGridView1.Visible = false;
            BotonGuardardatostrabajadoresclientes.Visible = false;
            EtiquetadeAdbertenciadeSeleccion.Visible = false;
            Facturaono.Visible = false;
            checkboxTieneTrabajadores.Visible = false;
            groupBoxTienesAdeudos.Visible = false;
            groupBox1.Visible = false;
        }

        private void contabilidadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GuadarDatosConCaducidad.Visible = false;
            ModificarVenta.Visible = false;
            BorrardeVneta.Visible = false;
            AgregarVenta.Visible = false;
            dataGridView1conCaducidad.Visible = false;
            Busqueda.Visible = false;
            GroupBoxManejaProductosPereceders.Visible = false;
            BotonAgregarProducto.Visible = false;
            BotonDardeBajaProducto.Visible = false;
            BotonGuardarCambios.Visible = false;
            groupBoxDarDeBajaEsteProducto.Visible = false;
            dataGridView2Modificardatos.Visible = false;
            BotonFinalizarventaConFactura.Visible = false;
            BotonFinalizarVentaConNota.Visible = false;
            BotonFinalizarVentaSimple.Visible = false;
            dataGridView2ParalaVenta.Visible = false;
            dataGridView2Ventassimples.Visible = false;
            dataGridView2TrabajadorasClientes.Visible = false;
            dataGridView1.Visible = false;
            BotonGuardardatostrabajadoresclientes.Visible = false;
            EtiquetadeAdbertenciadeSeleccion.Visible = false;
            Facturaono.Visible = false;
            checkboxTieneTrabajadores.Visible = false;
            groupBoxTienesAdeudos.Visible = false;
            groupBox1.Visible = false;
        }

        private void menuStrip2_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void gastosContinuosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GuadarDatosConCaducidad.Visible = false;
            ModificarVenta.Visible = false;
            BorrardeVneta.Visible = false;
            AgregarVenta.Visible = false;
            Busqueda.Visible = false;
            dataGridView1conCaducidad.Visible = false;
        }

        private void adeudosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GuadarDatosConCaducidad.Visible = false;
            ModificarVenta.Visible = false;
            BorrardeVneta.Visible = false;
            AgregarVenta.Visible = false;
            dataGridView1conCaducidad.Visible = false;
        }

        private void BotonGuardarProductos_Click(object sender, EventArgs e)
        {
            string Cadena = "data source = DESKTOP-S2699I6; initial catalog = Tesis1LoquenecesitaelUsuario1; user id = SA; password = 123";
            SqlConnection conec = new SqlConnection(Cadena);

            SqlCommand agregar = new SqlCommand("insert into Producto values (@CodigoBarras, @NombreGenerico, @TipoProducto, @MarcaProducto, @PrecioCompra, @PrecoVenta, @CantidadProducto, @CantidadMInProducto, @UnidaddemedidaProductos, @FechaCompra)", conec);
            conec.Open();
            try
            {
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    agregar.Parameters.Clear();

                    agregar.Parameters.AddWithValue("@CodigoBarras", Convert.ToString(row.Cells["CodigoBarras"].Value));
                    agregar.Parameters.AddWithValue("@NombreGenerico", Convert.ToString(row.Cells["NombreGenerico"].Value));
                    agregar.Parameters.AddWithValue("@TipoProducto", Convert.ToString(row.Cells["TipoProducto"].Value));
                    agregar.Parameters.AddWithValue("@MarcaProducto", Convert.ToString(row.Cells["MarcaProduco"].Value));
                    agregar.Parameters.AddWithValue("@PrecioCompra", Convert.ToString(row.Cells["PrecioCompra"].Value));
                    agregar.Parameters.AddWithValue("@PrecoVenta", Convert.ToString(row.Cells["PrecioVenta"].Value));
                    agregar.Parameters.AddWithValue("@CantidadProducto", Convert.ToString(row.Cells["CantidadProducto"].Value));
                    agregar.Parameters.AddWithValue("@CantidadMInProducto", Convert.ToString(row.Cells["CantidadMinProducto"].Value));
                    agregar.Parameters.AddWithValue("@UnidaddemedidaProductos", Convert.ToString(row.Cells["UnidadMedida"].Value));
                    agregar.Parameters.AddWithValue("@FechaCompra", Convert.ToString(row.Cells["FechaCompra"].Value));
                    agregar.ExecuteNonQuery();
                }
                MessageBox.Show("Datos agregados");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex.Message);
            }
            finally
            {
                conec.Close();
            }
        }

        private void BotonGuardarDatosGenerales_Click(object sender, EventArgs e)
        {
            if (FacturaSi.Checked)
            {
                ventaConFacturaToolStripMenuItem.Enabled = true;
            }
            else
            {
                ventaConFacturaToolStripMenuItem.Enabled = false;
            }
            if(FacturaSi.Checked && TienetrabajadoresCheckbox.Checked)//habilitar o des-habilitar en el menu trabajadores y clientes
            {
                clientesToolStripMenuItem1.Enabled = false;
            }
            else
            {
                clientesToolStripMenuItem1.Enabled = true;
            }
            if(checkBox2.Checked)
            {
                dataGridView1conCaducidad.Enabled = true;
                GuadarDatosConCaducidad.Enabled = true;
                dataGridView1.Enabled = false;
                BotonGuardarProductos.Enabled = false;
            }
            else
            {
                dataGridView1conCaducidad.Enabled = false;
                GuadarDatosConCaducidad.Enabled = false;
                dataGridView1.Enabled = true;
                BotonGuardarProductos.Enabled = true;
            }
            ventasToolStripMenuItem.Enabled = true;
            comprasToolStripMenuItem.Enabled = true;
            llenadoDeDatosToolStripMenuItem.Enabled = true;
            contabilidadToolStripMenuItem.Enabled = true;
            Facturaono.Enabled = false;
            checkboxTieneTrabajadores.Enabled = false;
            groupBoxTienesAdeudos.Enabled = false;
            groupBox1.Enabled = false;
            GroupBoxManejaProductosPereceders.Enabled = false;
            try
            {
                string Cadena = "data source = DESKTOP-S2699I6; initial catalog = Tesis1LoquenecesitaelUsuario1; user id = SA; password = 123";
                SqlConnection conec = new SqlConnection(Cadena);

                conec.Open();

                string query = "INSERT       INTO              Controlar(CaducidadProducto, Facturacion, TienesTrabajadores, SinAdeudos, TienenAdedosAusted) VALUES('@CaducidadProducto', '@Facturacion', '@TienesTrabajadores', '@SinAdeudos', '@TienenAdedosAusted')";

                string qrytemp = query;
                
                qrytemp = qrytemp.Replace("@CaducidadProducto", checkBox2.Checked.ToString());
                qrytemp = qrytemp.Replace("@Facturacion", FacturaSi.Checked.ToString());
                qrytemp = qrytemp.Replace("@TienesTrabajadores", TienetrabajadoresCheckbox.Checked.ToString());
                qrytemp = qrytemp.Replace("@SinAdeudos", TienesAdeudosCheckBox.Checked.ToString());
                qrytemp = qrytemp.Replace("@TienenAdedosAusted", checkBox1.Checked.ToString());
                
                SqlCommand cmd = new SqlCommand(qrytemp, conec);
                
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex.Message);
            }
        }

        private void BotonGuardardatostrabajadoresclientes_Click(object sender, EventArgs e)
        {
            string Cadena = "data source = DESKTOP-S2699I6; initial catalog = Tesis1LoquenecesitaelUsuario1; user id = SA; password = 123";
            SqlConnection conec = new SqlConnection(Cadena);

            SqlCommand agregar = new SqlCommand("insert into DatosPersonas values (@RFCCliente, @ApehidoPeternoCliente, @ApehidoMaternoCliente, @NombreCliente, @NumTelefonicoCiente, @Numtelcelular, @DireccionFiscal, @email, @Trabajadorocliente, @Registrados, @sueldo, @ISR)", conec);
            conec.Open();
            try
            {
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    agregar.Parameters.Clear();

                    agregar.Parameters.AddWithValue("@RFCCliente", Convert.ToString(row.Cells["RFCPersona"].Value));
                    agregar.Parameters.AddWithValue("@ApehidoPeternoCliente", Convert.ToString(row.Cells["ApehidoPaterno"].Value));
                    agregar.Parameters.AddWithValue("@ApehidoMaternoCliente", Convert.ToString(row.Cells["ApehidoMaterno"].Value));
                    agregar.Parameters.AddWithValue("@NombreCliente", Convert.ToString(row.Cells["Nombre"].Value));
                    agregar.Parameters.AddWithValue("@NumTelefonicoCiente", Convert.ToString(row.Cells["NumeroTelef"].Value));
                    agregar.Parameters.AddWithValue("@Numtelcelular", Convert.ToString(row.Cells["NumCelular"].Value));
                    agregar.Parameters.AddWithValue("@DireccionFiscal", Convert.ToString(row.Cells["DireccionFiscal"].Value));
                    agregar.Parameters.AddWithValue("@email", Convert.ToString(row.Cells["Email"].Value));
                    agregar.Parameters.AddWithValue("@Trabajadorocliente", Convert.ToString(row.Cells["TabajadorCliente"].Value));
                    agregar.Parameters.AddWithValue("@Registrados", Convert.ToString(row.Cells["Registrados"].Value));
                    agregar.Parameters.AddWithValue("@sueldo", Convert.ToString(row.Cells["Sueldo"].Value));
                    agregar.Parameters.AddWithValue("@ISR", Convert.ToString(row.Cells["ISRaPagar"].Value));
                    agregar.ExecuteNonQuery();
                }
                MessageBox.Show("Datos agregados");
            }
            catch (Exception ex)
            {
                MessageBox.Show("error");
            }
            finally
            {
                conec.Close();
            }
        }

        private void GuadarDatosConCaducidad_Click(object sender, EventArgs e)
        {
            string Cadena = "data source = DESKTOP-S2699I6; initial catalog = Tesis1LoquenecesitaelUsuario1; user id = SA; password = 123";
            SqlConnection conec = new SqlConnection(Cadena);

            SqlCommand agregar = new SqlCommand("insert into Producto values (@CodigoBarras, @NombreGenerico, @TipoProducto, @MarcaProducto, @PrecioCompra, @PrecoVenta, @CantidadProducto, @CantidadMInProducto, @UnidaddemedidaProductos, @FechaCompra, @fechaCaducidad)", conec);
            conec.Open();
            try
            {
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    agregar.Parameters.Clear();

                    agregar.Parameters.AddWithValue("@CodigoBarras", Convert.ToString(row.Cells["CodBarrasCaducidad"].Value));
                    agregar.Parameters.AddWithValue("@NombreGenerico", Convert.ToString(row.Cells["NombreGenericoCodBarras"].Value));
                    agregar.Parameters.AddWithValue("@TipoProducto", Convert.ToString(row.Cells["TipoProductoCodBarras"].Value));
                    agregar.Parameters.AddWithValue("@MarcaProducto", Convert.ToString(row.Cells["MarcaPtoductoCodBarras"].Value));
                    agregar.Parameters.AddWithValue("@PrecioCompra", Convert.ToString(row.Cells["PrecioCompraCodBarras"].Value));
                    agregar.Parameters.AddWithValue("@PrecoVenta", Convert.ToString(row.Cells["PrecioVnetaCodBarras"].Value));
                    agregar.Parameters.AddWithValue("@CantidadProducto", Convert.ToString(row.Cells["CantidadProductoCodBarras"].Value));
                    agregar.Parameters.AddWithValue("@CantidadMInProducto", Convert.ToString(row.Cells["CantidadMinProductoCodBarras"].Value));
                    agregar.Parameters.AddWithValue("@UnidaddemedidaProductos", Convert.ToString(row.Cells["UnidadMedidaCodBarras"].Value));
                    agregar.Parameters.AddWithValue("@FechaCompra", Convert.ToString(row.Cells["FechaCompraCodBarras"].Value));
                    agregar.Parameters.AddWithValue("@fechaCaducidad", Convert.ToString(row.Cells["FechaCaducidadCodBarras"].Value));
                    agregar.ExecuteNonQuery();
                }
                MessageBox.Show("Datos agregados");
            }
            catch (Exception ex)
            {
                MessageBox.Show("error");
            }
            finally
            {
                conec.Close();
            }
        }

        private void FacturaSi_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void TienetrabajadoresCheckbox_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void TienesAdeudosCheckBox_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void BusquedaDeProducto_Click(object sender, EventArgs e)
        {

        }

        private void Busquedapornombre_TextChanged(object sender, EventArgs e)
        {

        }
    }
}