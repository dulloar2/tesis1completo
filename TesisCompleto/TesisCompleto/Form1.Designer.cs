﻿namespace TesisCompleto
{
    partial class Menu
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.MenuStrip menuStrip2;
            this.datosBasicosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ventasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ventaSimpleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ventaConNotaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ventaConFacturaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comprasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarProductoExistenteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.darDeBajaUnProductoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarNuevoProductoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.llenadoDeDatosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.clientesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.empresaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contabilidadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adeudosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gastosContinuosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.CodigoBarras = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NombreGenerico = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoProducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MarcaProduco = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrecioCompra = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrecioVenta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CantidadProducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CantidadMinProducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UnidadMedida = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaCompra = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BotonGuardarProductos = new System.Windows.Forms.Button();
            this.dataGridView2TrabajadorasClientes = new System.Windows.Forms.DataGridView();
            this.RFCPersona = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ApehidoPaterno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ApehidoMaterno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumeroTelef = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumCelular = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DireccionFiscal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TabajadorCliente = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Registrados = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Sueldo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ISRaPagar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BotonGuardardatostrabajadoresclientes = new System.Windows.Forms.Button();
            this.Facturaono = new System.Windows.Forms.GroupBox();
            this.FacturaSi = new System.Windows.Forms.CheckBox();
            this.checkboxTieneTrabajadores = new System.Windows.Forms.GroupBox();
            this.TienetrabajadoresCheckbox = new System.Windows.Forms.CheckBox();
            this.groupBoxTienesAdeudos = new System.Windows.Forms.GroupBox();
            this.TienesAdeudosCheckBox = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.BotonGuardarDatosGenerales = new System.Windows.Forms.Button();
            this.EtiquetadeAdbertenciadeSeleccion = new System.Windows.Forms.Label();
            this.dataGridView2Ventassimples = new System.Windows.Forms.DataGridView();
            this.CodigodeBarras = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NombreGenericos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoProductos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MarcaProductos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PreciodVenta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CantidadProductos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView2ParalaVenta = new System.Windows.Forms.DataGridView();
            this.CodigoBarrasVenderSimple = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NombreGenericoaVender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoProductoaVender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MarcaProductoVender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrecioVentaVendido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CantidadProductoVenta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BotonFinalizarVentaSimple = new System.Windows.Forms.Button();
            this.BotonFinalizarVentaConNota = new System.Windows.Forms.Button();
            this.BotonFinalizarventaConFactura = new System.Windows.Forms.Button();
            this.dataGridView2Modificardatos = new System.Windows.Forms.DataGridView();
            this.groupBoxDarDeBajaEsteProducto = new System.Windows.Forms.GroupBox();
            this.checkBoxdardeBajaEsteProducto = new System.Windows.Forms.CheckBox();
            this.BotonGuardarCambios = new System.Windows.Forms.Button();
            this.BotonDardeBajaProducto = new System.Windows.Forms.Button();
            this.BotonAgregarProducto = new System.Windows.Forms.Button();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.GroupBoxManejaProductosPereceders = new System.Windows.Forms.GroupBox();
            this.dataGridView1conCaducidad = new System.Windows.Forms.DataGridView();
            this.CodBarrasCaducidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NombreGenericoCodBarras = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoProductoCodBarras = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MarcaPtoductoCodBarras = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrecioCompraCodBarras = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrecioVnetaCodBarras = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CantidadProductoCodBarras = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CantidadMinProductoCodBarras = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UnidadMedidaCodBarras = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaCompraCodBarras = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaCaducidadCodBarras = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Busqueda = new System.Windows.Forms.GroupBox();
            this.BusquedaDeProducto = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.PorCodigoBarras = new System.Windows.Forms.CheckBox();
            this.BusquedaPormarca = new System.Windows.Forms.TextBox();
            this.PorMarca = new System.Windows.Forms.CheckBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.PortipoProducto = new System.Windows.Forms.CheckBox();
            this.Busquedapornombre = new System.Windows.Forms.TextBox();
            this.PorNombre = new System.Windows.Forms.CheckBox();
            this.AgregarVenta = new System.Windows.Forms.Button();
            this.BorrardeVneta = new System.Windows.Forms.Button();
            this.ModificarVenta = new System.Windows.Forms.Button();
            this.GuadarDatosConCaducidad = new System.Windows.Forms.Button();
            menuStrip2 = new System.Windows.Forms.MenuStrip();
            menuStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2TrabajadorasClientes)).BeginInit();
            this.Facturaono.SuspendLayout();
            this.checkboxTieneTrabajadores.SuspendLayout();
            this.groupBoxTienesAdeudos.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2Ventassimples)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2ParalaVenta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2Modificardatos)).BeginInit();
            this.groupBoxDarDeBajaEsteProducto.SuspendLayout();
            this.GroupBoxManejaProductosPereceders.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1conCaducidad)).BeginInit();
            this.Busqueda.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip2
            // 
            menuStrip2.AllowItemReorder = true;
            menuStrip2.Dock = System.Windows.Forms.DockStyle.Left;
            menuStrip2.ImeMode = System.Windows.Forms.ImeMode.Off;
            menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.datosBasicosToolStripMenuItem,
            this.ventasToolStripMenuItem,
            this.comprasToolStripMenuItem,
            this.llenadoDeDatosToolStripMenuItem,
            this.contabilidadToolStripMenuItem});
            menuStrip2.Location = new System.Drawing.Point(0, 0);
            menuStrip2.Name = "menuStrip2";
            menuStrip2.ShowItemToolTips = true;
            menuStrip2.Size = new System.Drawing.Size(190, 498);
            menuStrip2.Stretch = false;
            menuStrip2.TabIndex = 8;
            menuStrip2.Text = "menuStrip2";
            menuStrip2.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip2_ItemClicked);
            // 
            // datosBasicosToolStripMenuItem
            // 
            this.datosBasicosToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datosBasicosToolStripMenuItem.Name = "datosBasicosToolStripMenuItem";
            this.datosBasicosToolStripMenuItem.Size = new System.Drawing.Size(177, 34);
            this.datosBasicosToolStripMenuItem.Text = "Datos basicos";
            this.datosBasicosToolStripMenuItem.Click += new System.EventHandler(this.datosBasicosToolStripMenuItem_Click);
            // 
            // ventasToolStripMenuItem
            // 
            this.ventasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ventaSimpleToolStripMenuItem,
            this.ventaConNotaToolStripMenuItem,
            this.ventaConFacturaToolStripMenuItem});
            this.ventasToolStripMenuItem.Enabled = false;
            this.ventasToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ventasToolStripMenuItem.Name = "ventasToolStripMenuItem";
            this.ventasToolStripMenuItem.Size = new System.Drawing.Size(177, 34);
            this.ventasToolStripMenuItem.Text = "Ventas";
            this.ventasToolStripMenuItem.Click += new System.EventHandler(this.ventasToolStripMenuItem_Click);
            // 
            // ventaSimpleToolStripMenuItem
            // 
            this.ventaSimpleToolStripMenuItem.Name = "ventaSimpleToolStripMenuItem";
            this.ventaSimpleToolStripMenuItem.Size = new System.Drawing.Size(250, 34);
            this.ventaSimpleToolStripMenuItem.Text = "Venta simple";
            this.ventaSimpleToolStripMenuItem.Visible = false;
            this.ventaSimpleToolStripMenuItem.Click += new System.EventHandler(this.ventaSimpleToolStripMenuItem_Click);
            // 
            // ventaConNotaToolStripMenuItem
            // 
            this.ventaConNotaToolStripMenuItem.Name = "ventaConNotaToolStripMenuItem";
            this.ventaConNotaToolStripMenuItem.Size = new System.Drawing.Size(250, 34);
            this.ventaConNotaToolStripMenuItem.Text = "Venta con nota";
            this.ventaConNotaToolStripMenuItem.Visible = false;
            this.ventaConNotaToolStripMenuItem.Click += new System.EventHandler(this.ventaConNotaToolStripMenuItem_Click);
            // 
            // ventaConFacturaToolStripMenuItem
            // 
            this.ventaConFacturaToolStripMenuItem.Name = "ventaConFacturaToolStripMenuItem";
            this.ventaConFacturaToolStripMenuItem.Size = new System.Drawing.Size(250, 34);
            this.ventaConFacturaToolStripMenuItem.Text = "Venta con factura";
            this.ventaConFacturaToolStripMenuItem.Visible = false;
            this.ventaConFacturaToolStripMenuItem.Click += new System.EventHandler(this.ventaConFacturaToolStripMenuItem_Click);
            // 
            // comprasToolStripMenuItem
            // 
            this.comprasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modificarProductoExistenteToolStripMenuItem,
            this.darDeBajaUnProductoToolStripMenuItem,
            this.agregarNuevoProductoToolStripMenuItem});
            this.comprasToolStripMenuItem.Enabled = false;
            this.comprasToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comprasToolStripMenuItem.Name = "comprasToolStripMenuItem";
            this.comprasToolStripMenuItem.Size = new System.Drawing.Size(177, 34);
            this.comprasToolStripMenuItem.Text = "Compras";
            this.comprasToolStripMenuItem.Click += new System.EventHandler(this.comprasToolStripMenuItem_Click);
            // 
            // modificarProductoExistenteToolStripMenuItem
            // 
            this.modificarProductoExistenteToolStripMenuItem.Name = "modificarProductoExistenteToolStripMenuItem";
            this.modificarProductoExistenteToolStripMenuItem.Size = new System.Drawing.Size(353, 34);
            this.modificarProductoExistenteToolStripMenuItem.Text = "Modificar producto existente";
            this.modificarProductoExistenteToolStripMenuItem.Click += new System.EventHandler(this.modificarProductoExistenteToolStripMenuItem_Click);
            // 
            // darDeBajaUnProductoToolStripMenuItem
            // 
            this.darDeBajaUnProductoToolStripMenuItem.Name = "darDeBajaUnProductoToolStripMenuItem";
            this.darDeBajaUnProductoToolStripMenuItem.Size = new System.Drawing.Size(353, 34);
            this.darDeBajaUnProductoToolStripMenuItem.Text = "Dar de baja un producto";
            this.darDeBajaUnProductoToolStripMenuItem.Click += new System.EventHandler(this.darDeBajaUnProductoToolStripMenuItem_Click);
            // 
            // agregarNuevoProductoToolStripMenuItem
            // 
            this.agregarNuevoProductoToolStripMenuItem.Name = "agregarNuevoProductoToolStripMenuItem";
            this.agregarNuevoProductoToolStripMenuItem.Size = new System.Drawing.Size(353, 34);
            this.agregarNuevoProductoToolStripMenuItem.Text = "Agregar nuevo  producto";
            this.agregarNuevoProductoToolStripMenuItem.Click += new System.EventHandler(this.agregarNuevoProductoToolStripMenuItem_Click);
            // 
            // llenadoDeDatosToolStripMenuItem
            // 
            this.llenadoDeDatosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.productosToolStripMenuItem1,
            this.clientesToolStripMenuItem1,
            this.empresaToolStripMenuItem});
            this.llenadoDeDatosToolStripMenuItem.Enabled = false;
            this.llenadoDeDatosToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.llenadoDeDatosToolStripMenuItem.Name = "llenadoDeDatosToolStripMenuItem";
            this.llenadoDeDatosToolStripMenuItem.Size = new System.Drawing.Size(177, 34);
            this.llenadoDeDatosToolStripMenuItem.Text = "Llenado de datos";
            this.llenadoDeDatosToolStripMenuItem.Click += new System.EventHandler(this.llenadoDeDatosToolStripMenuItem_Click);
            // 
            // productosToolStripMenuItem1
            // 
            this.productosToolStripMenuItem1.Name = "productosToolStripMenuItem1";
            this.productosToolStripMenuItem1.Size = new System.Drawing.Size(298, 34);
            this.productosToolStripMenuItem1.Text = "Productos";
            this.productosToolStripMenuItem1.Click += new System.EventHandler(this.productosToolStripMenuItem1_Click);
            // 
            // clientesToolStripMenuItem1
            // 
            this.clientesToolStripMenuItem1.Name = "clientesToolStripMenuItem1";
            this.clientesToolStripMenuItem1.Size = new System.Drawing.Size(298, 34);
            this.clientesToolStripMenuItem1.Text = "Clientes y Trabajadores";
            this.clientesToolStripMenuItem1.Click += new System.EventHandler(this.clientesToolStripMenuItem1_Click);
            // 
            // empresaToolStripMenuItem
            // 
            this.empresaToolStripMenuItem.Name = "empresaToolStripMenuItem";
            this.empresaToolStripMenuItem.Size = new System.Drawing.Size(298, 34);
            this.empresaToolStripMenuItem.Text = "Datos de la empresa";
            this.empresaToolStripMenuItem.Click += new System.EventHandler(this.empresaToolStripMenuItem_Click);
            // 
            // contabilidadToolStripMenuItem
            // 
            this.contabilidadToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.adeudosToolStripMenuItem,
            this.gastosContinuosToolStripMenuItem});
            this.contabilidadToolStripMenuItem.Enabled = false;
            this.contabilidadToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.contabilidadToolStripMenuItem.Name = "contabilidadToolStripMenuItem";
            this.contabilidadToolStripMenuItem.Size = new System.Drawing.Size(177, 34);
            this.contabilidadToolStripMenuItem.Text = "Contabilidad";
            this.contabilidadToolStripMenuItem.Click += new System.EventHandler(this.contabilidadToolStripMenuItem_Click);
            // 
            // adeudosToolStripMenuItem
            // 
            this.adeudosToolStripMenuItem.Name = "adeudosToolStripMenuItem";
            this.adeudosToolStripMenuItem.Size = new System.Drawing.Size(245, 34);
            this.adeudosToolStripMenuItem.Text = "Adeudos";
            this.adeudosToolStripMenuItem.Click += new System.EventHandler(this.adeudosToolStripMenuItem_Click);
            // 
            // gastosContinuosToolStripMenuItem
            // 
            this.gastosContinuosToolStripMenuItem.Name = "gastosContinuosToolStripMenuItem";
            this.gastosContinuosToolStripMenuItem.Size = new System.Drawing.Size(245, 34);
            this.gastosContinuosToolStripMenuItem.Text = "Gastos continuos";
            this.gastosContinuosToolStripMenuItem.Click += new System.EventHandler(this.gastosContinuosToolStripMenuItem_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CodigoBarras,
            this.NombreGenerico,
            this.TipoProducto,
            this.MarcaProduco,
            this.PrecioCompra,
            this.PrecioVenta,
            this.CantidadProducto,
            this.CantidadMinProducto,
            this.UnidadMedida,
            this.FechaCompra});
            this.dataGridView1.Enabled = false;
            this.dataGridView1.Location = new System.Drawing.Point(12, 175);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(65, 106);
            this.dataGridView1.TabIndex = 9;
            this.dataGridView1.Visible = false;
            // 
            // CodigoBarras
            // 
            this.CodigoBarras.HeaderText = "Codigo de barras";
            this.CodigoBarras.Name = "CodigoBarras";
            // 
            // NombreGenerico
            // 
            this.NombreGenerico.HeaderText = "Nombre generico";
            this.NombreGenerico.Name = "NombreGenerico";
            // 
            // TipoProducto
            // 
            this.TipoProducto.HeaderText = "Tipo de prodructo ";
            this.TipoProducto.Name = "TipoProducto";
            // 
            // MarcaProduco
            // 
            this.MarcaProduco.HeaderText = "Marca de producto";
            this.MarcaProduco.Name = "MarcaProduco";
            // 
            // PrecioCompra
            // 
            this.PrecioCompra.HeaderText = "Preci de compra";
            this.PrecioCompra.Name = "PrecioCompra";
            // 
            // PrecioVenta
            // 
            this.PrecioVenta.HeaderText = "Precio de venta";
            this.PrecioVenta.Name = "PrecioVenta";
            // 
            // CantidadProducto
            // 
            this.CantidadProducto.HeaderText = "Cantidad de producto";
            this.CantidadProducto.Name = "CantidadProducto";
            // 
            // CantidadMinProducto
            // 
            this.CantidadMinProducto.HeaderText = "Cantidad minima de producto";
            this.CantidadMinProducto.Name = "CantidadMinProducto";
            // 
            // UnidadMedida
            // 
            this.UnidadMedida.HeaderText = "Unidad de medida";
            this.UnidadMedida.Name = "UnidadMedida";
            // 
            // FechaCompra
            // 
            this.FechaCompra.HeaderText = "Fecha de compra";
            this.FechaCompra.Name = "FechaCompra";
            // 
            // BotonGuardarProductos
            // 
            this.BotonGuardarProductos.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotonGuardarProductos.Location = new System.Drawing.Point(1068, 429);
            this.BotonGuardarProductos.Name = "BotonGuardarProductos";
            this.BotonGuardarProductos.Size = new System.Drawing.Size(274, 56);
            this.BotonGuardarProductos.TabIndex = 10;
            this.BotonGuardarProductos.Text = "Guardar datos";
            this.BotonGuardarProductos.UseVisualStyleBackColor = true;
            this.BotonGuardarProductos.Visible = false;
            this.BotonGuardarProductos.Click += new System.EventHandler(this.BotonGuardarProductos_Click);
            // 
            // dataGridView2TrabajadorasClientes
            // 
            this.dataGridView2TrabajadorasClientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2TrabajadorasClientes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RFCPersona,
            this.ApehidoPaterno,
            this.ApehidoMaterno,
            this.Nombre,
            this.NumeroTelef,
            this.NumCelular,
            this.DireccionFiscal,
            this.Email,
            this.TabajadorCliente,
            this.Registrados,
            this.Sueldo,
            this.ISRaPagar});
            this.dataGridView2TrabajadorasClientes.Location = new System.Drawing.Point(83, 175);
            this.dataGridView2TrabajadorasClientes.Name = "dataGridView2TrabajadorasClientes";
            this.dataGridView2TrabajadorasClientes.Size = new System.Drawing.Size(65, 106);
            this.dataGridView2TrabajadorasClientes.TabIndex = 11;
            this.dataGridView2TrabajadorasClientes.Visible = false;
            // 
            // RFCPersona
            // 
            this.RFCPersona.HeaderText = "RFC";
            this.RFCPersona.Name = "RFCPersona";
            // 
            // ApehidoPaterno
            // 
            this.ApehidoPaterno.HeaderText = "Apehido paterno";
            this.ApehidoPaterno.Name = "ApehidoPaterno";
            // 
            // ApehidoMaterno
            // 
            this.ApehidoMaterno.HeaderText = "Apehido materno";
            this.ApehidoMaterno.Name = "ApehidoMaterno";
            // 
            // Nombre
            // 
            this.Nombre.HeaderText = "Nombe";
            this.Nombre.Name = "Nombre";
            // 
            // NumeroTelef
            // 
            this.NumeroTelef.HeaderText = "Numero telefonico local";
            this.NumeroTelef.Name = "NumeroTelef";
            // 
            // NumCelular
            // 
            this.NumCelular.HeaderText = "Numero telefonico celular";
            this.NumCelular.Name = "NumCelular";
            // 
            // DireccionFiscal
            // 
            this.DireccionFiscal.HeaderText = "Direccion Fiscal";
            this.DireccionFiscal.Name = "DireccionFiscal";
            // 
            // Email
            // 
            this.Email.HeaderText = "Email";
            this.Email.Name = "Email";
            // 
            // TabajadorCliente
            // 
            this.TabajadorCliente.HeaderText = "Trabajador o cliente";
            this.TabajadorCliente.Name = "TabajadorCliente";
            // 
            // Registrados
            // 
            this.Registrados.HeaderText = "¿El tabajador esta registrado?";
            this.Registrados.Name = "Registrados";
            // 
            // Sueldo
            // 
            this.Sueldo.HeaderText = "Sueldo";
            this.Sueldo.Name = "Sueldo";
            // 
            // ISRaPagar
            // 
            this.ISRaPagar.HeaderText = "ISR a pagar";
            this.ISRaPagar.Name = "ISRaPagar";
            this.ISRaPagar.ReadOnly = true;
            // 
            // BotonGuardardatostrabajadoresclientes
            // 
            this.BotonGuardardatostrabajadoresclientes.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotonGuardardatostrabajadoresclientes.Location = new System.Drawing.Point(1068, 364);
            this.BotonGuardardatostrabajadoresclientes.Name = "BotonGuardardatostrabajadoresclientes";
            this.BotonGuardardatostrabajadoresclientes.Size = new System.Drawing.Size(274, 56);
            this.BotonGuardardatostrabajadoresclientes.TabIndex = 12;
            this.BotonGuardardatostrabajadoresclientes.Text = "Guardar datos T y C";
            this.BotonGuardardatostrabajadoresclientes.UseVisualStyleBackColor = true;
            this.BotonGuardardatostrabajadoresclientes.Visible = false;
            this.BotonGuardardatostrabajadoresclientes.Click += new System.EventHandler(this.BotonGuardardatostrabajadoresclientes_Click);
            // 
            // Facturaono
            // 
            this.Facturaono.Controls.Add(this.FacturaSi);
            this.Facturaono.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Facturaono.Location = new System.Drawing.Point(183, 59);
            this.Facturaono.Name = "Facturaono";
            this.Facturaono.Size = new System.Drawing.Size(146, 44);
            this.Facturaono.TabIndex = 13;
            this.Facturaono.TabStop = false;
            this.Facturaono.Text = "¿Usted factura?";
            this.Facturaono.Visible = false;
            // 
            // FacturaSi
            // 
            this.FacturaSi.AutoSize = true;
            this.FacturaSi.Location = new System.Drawing.Point(61, 19);
            this.FacturaSi.Name = "FacturaSi";
            this.FacturaSi.Size = new System.Drawing.Size(15, 14);
            this.FacturaSi.TabIndex = 0;
            this.FacturaSi.UseVisualStyleBackColor = true;
            this.FacturaSi.CheckedChanged += new System.EventHandler(this.FacturaSi_CheckedChanged);
            // 
            // checkboxTieneTrabajadores
            // 
            this.checkboxTieneTrabajadores.Controls.Add(this.TienetrabajadoresCheckbox);
            this.checkboxTieneTrabajadores.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkboxTieneTrabajadores.Location = new System.Drawing.Point(335, 59);
            this.checkboxTieneTrabajadores.Name = "checkboxTieneTrabajadores";
            this.checkboxTieneTrabajadores.Size = new System.Drawing.Size(173, 44);
            this.checkboxTieneTrabajadores.TabIndex = 14;
            this.checkboxTieneTrabajadores.TabStop = false;
            this.checkboxTieneTrabajadores.Text = "¿Tiene trabajadores?";
            this.checkboxTieneTrabajadores.Visible = false;
            // 
            // TienetrabajadoresCheckbox
            // 
            this.TienetrabajadoresCheckbox.AutoSize = true;
            this.TienetrabajadoresCheckbox.Location = new System.Drawing.Point(84, 19);
            this.TienetrabajadoresCheckbox.Name = "TienetrabajadoresCheckbox";
            this.TienetrabajadoresCheckbox.Size = new System.Drawing.Size(15, 14);
            this.TienetrabajadoresCheckbox.TabIndex = 0;
            this.TienetrabajadoresCheckbox.UseVisualStyleBackColor = true;
            this.TienetrabajadoresCheckbox.CheckedChanged += new System.EventHandler(this.TienetrabajadoresCheckbox_CheckedChanged);
            // 
            // groupBoxTienesAdeudos
            // 
            this.groupBoxTienesAdeudos.Controls.Add(this.TienesAdeudosCheckBox);
            this.groupBoxTienesAdeudos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxTienesAdeudos.Location = new System.Drawing.Point(514, 59);
            this.groupBoxTienesAdeudos.Name = "groupBoxTienesAdeudos";
            this.groupBoxTienesAdeudos.Size = new System.Drawing.Size(306, 44);
            this.groupBoxTienesAdeudos.TabIndex = 15;
            this.groupBoxTienesAdeudos.TabStop = false;
            this.groupBoxTienesAdeudos.Text = "¿Tienes adeudos con  alguna empresa?";
            this.groupBoxTienesAdeudos.Visible = false;
            // 
            // TienesAdeudosCheckBox
            // 
            this.TienesAdeudosCheckBox.AutoSize = true;
            this.TienesAdeudosCheckBox.Location = new System.Drawing.Point(155, 19);
            this.TienesAdeudosCheckBox.Name = "TienesAdeudosCheckBox";
            this.TienesAdeudosCheckBox.Size = new System.Drawing.Size(15, 14);
            this.TienesAdeudosCheckBox.TabIndex = 0;
            this.TienesAdeudosCheckBox.UseVisualStyleBackColor = true;
            this.TienesAdeudosCheckBox.CheckedChanged += new System.EventHandler(this.TienesAdeudosCheckBox_CheckedChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(166, 25);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 16;
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(826, 59);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(333, 44);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "¿Alguna persona tiene adeudos con usted?";
            this.groupBox1.Visible = false;
            // 
            // BotonGuardarDatosGenerales
            // 
            this.BotonGuardarDatosGenerales.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotonGuardarDatosGenerales.Location = new System.Drawing.Point(1068, 291);
            this.BotonGuardarDatosGenerales.Name = "BotonGuardarDatosGenerales";
            this.BotonGuardarDatosGenerales.Size = new System.Drawing.Size(274, 56);
            this.BotonGuardarDatosGenerales.TabIndex = 18;
            this.BotonGuardarDatosGenerales.Text = "Guardar datos generales";
            this.BotonGuardarDatosGenerales.UseVisualStyleBackColor = true;
            this.BotonGuardarDatosGenerales.Visible = false;
            this.BotonGuardarDatosGenerales.Click += new System.EventHandler(this.BotonGuardarDatosGenerales_Click);
            // 
            // EtiquetadeAdbertenciadeSeleccion
            // 
            this.EtiquetadeAdbertenciadeSeleccion.AutoSize = true;
            this.EtiquetadeAdbertenciadeSeleccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EtiquetadeAdbertenciadeSeleccion.Location = new System.Drawing.Point(178, 9);
            this.EtiquetadeAdbertenciadeSeleccion.Name = "EtiquetadeAdbertenciadeSeleccion";
            this.EtiquetadeAdbertenciadeSeleccion.Size = new System.Drawing.Size(1006, 25);
            this.EtiquetadeAdbertenciadeSeleccion.TabIndex = 19;
            this.EtiquetadeAdbertenciadeSeleccion.Text = "Seleccione cualquiera de las opciones, recuerde que si no maneja algo de esto sol" +
    "o dejelo desmarcado";
            this.EtiquetadeAdbertenciadeSeleccion.Visible = false;
            // 
            // dataGridView2Ventassimples
            // 
            this.dataGridView2Ventassimples.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2Ventassimples.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CodigodeBarras,
            this.NombreGenericos,
            this.TipoProductos,
            this.MarcaProductos,
            this.PreciodVenta,
            this.CantidadProductos});
            this.dataGridView2Ventassimples.Location = new System.Drawing.Point(154, 175);
            this.dataGridView2Ventassimples.Name = "dataGridView2Ventassimples";
            this.dataGridView2Ventassimples.Size = new System.Drawing.Size(66, 106);
            this.dataGridView2Ventassimples.TabIndex = 20;
            this.dataGridView2Ventassimples.Visible = false;
            // 
            // CodigodeBarras
            // 
            this.CodigodeBarras.HeaderText = "Codigo de barras";
            this.CodigodeBarras.Name = "CodigodeBarras";
            // 
            // NombreGenericos
            // 
            this.NombreGenericos.HeaderText = "Nombre generico";
            this.NombreGenericos.Name = "NombreGenericos";
            // 
            // TipoProductos
            // 
            this.TipoProductos.HeaderText = "Tipo de producto";
            this.TipoProductos.Name = "TipoProductos";
            // 
            // MarcaProductos
            // 
            this.MarcaProductos.HeaderText = "Marca";
            this.MarcaProductos.Name = "MarcaProductos";
            // 
            // PreciodVenta
            // 
            this.PreciodVenta.HeaderText = "Precio de venta";
            this.PreciodVenta.Name = "PreciodVenta";
            // 
            // CantidadProductos
            // 
            this.CantidadProductos.HeaderText = "Cantidad de producto";
            this.CantidadProductos.Name = "CantidadProductos";
            // 
            // dataGridView2ParalaVenta
            // 
            this.dataGridView2ParalaVenta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2ParalaVenta.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CodigoBarrasVenderSimple,
            this.NombreGenericoaVender,
            this.TipoProductoaVender,
            this.MarcaProductoVender,
            this.PrecioVentaVendido,
            this.CantidadProductoVenta});
            this.dataGridView2ParalaVenta.Location = new System.Drawing.Point(226, 175);
            this.dataGridView2ParalaVenta.Name = "dataGridView2ParalaVenta";
            this.dataGridView2ParalaVenta.Size = new System.Drawing.Size(66, 106);
            this.dataGridView2ParalaVenta.TabIndex = 21;
            this.dataGridView2ParalaVenta.Visible = false;
            // 
            // CodigoBarrasVenderSimple
            // 
            this.CodigoBarrasVenderSimple.HeaderText = "Codigo de barras";
            this.CodigoBarrasVenderSimple.Name = "CodigoBarrasVenderSimple";
            // 
            // NombreGenericoaVender
            // 
            this.NombreGenericoaVender.HeaderText = "Nombre generico";
            this.NombreGenericoaVender.Name = "NombreGenericoaVender";
            // 
            // TipoProductoaVender
            // 
            this.TipoProductoaVender.HeaderText = "Tipo producto";
            this.TipoProductoaVender.Name = "TipoProductoaVender";
            // 
            // MarcaProductoVender
            // 
            this.MarcaProductoVender.HeaderText = "Marca";
            this.MarcaProductoVender.Name = "MarcaProductoVender";
            // 
            // PrecioVentaVendido
            // 
            this.PrecioVentaVendido.HeaderText = "Precio de venta";
            this.PrecioVentaVendido.Name = "PrecioVentaVendido";
            // 
            // CantidadProductoVenta
            // 
            this.CantidadProductoVenta.HeaderText = "Cantidad de producto";
            this.CantidadProductoVenta.Name = "CantidadProductoVenta";
            // 
            // BotonFinalizarVentaSimple
            // 
            this.BotonFinalizarVentaSimple.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotonFinalizarVentaSimple.Location = new System.Drawing.Point(877, 430);
            this.BotonFinalizarVentaSimple.Name = "BotonFinalizarVentaSimple";
            this.BotonFinalizarVentaSimple.Size = new System.Drawing.Size(175, 56);
            this.BotonFinalizarVentaSimple.TabIndex = 22;
            this.BotonFinalizarVentaSimple.Text = "Finalizar venta";
            this.BotonFinalizarVentaSimple.UseVisualStyleBackColor = true;
            this.BotonFinalizarVentaSimple.Visible = false;
            // 
            // BotonFinalizarVentaConNota
            // 
            this.BotonFinalizarVentaConNota.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotonFinalizarVentaConNota.Location = new System.Drawing.Point(877, 368);
            this.BotonFinalizarVentaConNota.Name = "BotonFinalizarVentaConNota";
            this.BotonFinalizarVentaConNota.Size = new System.Drawing.Size(175, 56);
            this.BotonFinalizarVentaConNota.TabIndex = 23;
            this.BotonFinalizarVentaConNota.Text = "Finalizar venta con nota";
            this.BotonFinalizarVentaConNota.UseVisualStyleBackColor = true;
            this.BotonFinalizarVentaConNota.Visible = false;
            // 
            // BotonFinalizarventaConFactura
            // 
            this.BotonFinalizarventaConFactura.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotonFinalizarventaConFactura.Location = new System.Drawing.Point(877, 306);
            this.BotonFinalizarventaConFactura.Name = "BotonFinalizarventaConFactura";
            this.BotonFinalizarventaConFactura.Size = new System.Drawing.Size(175, 56);
            this.BotonFinalizarventaConFactura.TabIndex = 24;
            this.BotonFinalizarventaConFactura.Text = "Finalizar venta";
            this.BotonFinalizarventaConFactura.UseVisualStyleBackColor = true;
            this.BotonFinalizarventaConFactura.Visible = false;
            // 
            // dataGridView2Modificardatos
            // 
            this.dataGridView2Modificardatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2Modificardatos.Location = new System.Drawing.Point(298, 175);
            this.dataGridView2Modificardatos.Name = "dataGridView2Modificardatos";
            this.dataGridView2Modificardatos.Size = new System.Drawing.Size(66, 106);
            this.dataGridView2Modificardatos.TabIndex = 25;
            this.dataGridView2Modificardatos.Visible = false;
            // 
            // groupBoxDarDeBajaEsteProducto
            // 
            this.groupBoxDarDeBajaEsteProducto.Controls.Add(this.checkBoxdardeBajaEsteProducto);
            this.groupBoxDarDeBajaEsteProducto.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxDarDeBajaEsteProducto.Location = new System.Drawing.Point(193, 109);
            this.groupBoxDarDeBajaEsteProducto.Name = "groupBoxDarDeBajaEsteProducto";
            this.groupBoxDarDeBajaEsteProducto.Size = new System.Drawing.Size(230, 38);
            this.groupBoxDarDeBajaEsteProducto.TabIndex = 26;
            this.groupBoxDarDeBajaEsteProducto.TabStop = false;
            this.groupBoxDarDeBajaEsteProducto.Text = "¿Dar de baja este producto?";
            this.groupBoxDarDeBajaEsteProducto.Visible = false;
            // 
            // checkBoxdardeBajaEsteProducto
            // 
            this.checkBoxdardeBajaEsteProducto.AutoSize = true;
            this.checkBoxdardeBajaEsteProducto.Location = new System.Drawing.Point(114, 18);
            this.checkBoxdardeBajaEsteProducto.Name = "checkBoxdardeBajaEsteProducto";
            this.checkBoxdardeBajaEsteProducto.Size = new System.Drawing.Size(15, 14);
            this.checkBoxdardeBajaEsteProducto.TabIndex = 0;
            this.checkBoxdardeBajaEsteProducto.UseVisualStyleBackColor = true;
            // 
            // BotonGuardarCambios
            // 
            this.BotonGuardarCambios.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotonGuardarCambios.Location = new System.Drawing.Point(877, 250);
            this.BotonGuardarCambios.Name = "BotonGuardarCambios";
            this.BotonGuardarCambios.Size = new System.Drawing.Size(175, 50);
            this.BotonGuardarCambios.TabIndex = 27;
            this.BotonGuardarCambios.Text = "Guardar cambios";
            this.BotonGuardarCambios.UseVisualStyleBackColor = true;
            this.BotonGuardarCambios.Visible = false;
            // 
            // BotonDardeBajaProducto
            // 
            this.BotonDardeBajaProducto.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotonDardeBajaProducto.Location = new System.Drawing.Point(877, 194);
            this.BotonDardeBajaProducto.Name = "BotonDardeBajaProducto";
            this.BotonDardeBajaProducto.Size = new System.Drawing.Size(175, 50);
            this.BotonDardeBajaProducto.TabIndex = 28;
            this.BotonDardeBajaProducto.Text = "Dar de baja";
            this.BotonDardeBajaProducto.UseVisualStyleBackColor = true;
            this.BotonDardeBajaProducto.Visible = false;
            // 
            // BotonAgregarProducto
            // 
            this.BotonAgregarProducto.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotonAgregarProducto.Location = new System.Drawing.Point(877, 138);
            this.BotonAgregarProducto.Name = "BotonAgregarProducto";
            this.BotonAgregarProducto.Size = new System.Drawing.Size(175, 50);
            this.BotonAgregarProducto.TabIndex = 29;
            this.BotonAgregarProducto.Text = "Agregar nuevo producto";
            this.BotonAgregarProducto.UseVisualStyleBackColor = true;
            this.BotonAgregarProducto.Visible = false;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(130, 18);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(15, 14);
            this.checkBox2.TabIndex = 30;
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // GroupBoxManejaProductosPereceders
            // 
            this.GroupBoxManejaProductosPereceders.Controls.Add(this.checkBox2);
            this.GroupBoxManejaProductosPereceders.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupBoxManejaProductosPereceders.Location = new System.Drawing.Point(429, 109);
            this.GroupBoxManejaProductosPereceders.Name = "GroupBoxManejaProductosPereceders";
            this.GroupBoxManejaProductosPereceders.Size = new System.Drawing.Size(265, 38);
            this.GroupBoxManejaProductosPereceders.TabIndex = 31;
            this.GroupBoxManejaProductosPereceders.TabStop = false;
            this.GroupBoxManejaProductosPereceders.Text = "¿Maneja productos perecederos?";
            this.GroupBoxManejaProductosPereceders.Visible = false;
            // 
            // dataGridView1conCaducidad
            // 
            this.dataGridView1conCaducidad.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1conCaducidad.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CodBarrasCaducidad,
            this.NombreGenericoCodBarras,
            this.TipoProductoCodBarras,
            this.MarcaPtoductoCodBarras,
            this.PrecioCompraCodBarras,
            this.PrecioVnetaCodBarras,
            this.CantidadProductoCodBarras,
            this.CantidadMinProductoCodBarras,
            this.UnidadMedidaCodBarras,
            this.FechaCompraCodBarras,
            this.FechaCaducidadCodBarras});
            this.dataGridView1conCaducidad.Enabled = false;
            this.dataGridView1conCaducidad.Location = new System.Drawing.Point(368, 175);
            this.dataGridView1conCaducidad.Name = "dataGridView1conCaducidad";
            this.dataGridView1conCaducidad.Size = new System.Drawing.Size(66, 106);
            this.dataGridView1conCaducidad.TabIndex = 32;
            this.dataGridView1conCaducidad.Visible = false;
            // 
            // CodBarrasCaducidad
            // 
            this.CodBarrasCaducidad.HeaderText = "Codigo de barras";
            this.CodBarrasCaducidad.Name = "CodBarrasCaducidad";
            // 
            // NombreGenericoCodBarras
            // 
            this.NombreGenericoCodBarras.HeaderText = "Nombre generico";
            this.NombreGenericoCodBarras.Name = "NombreGenericoCodBarras";
            // 
            // TipoProductoCodBarras
            // 
            this.TipoProductoCodBarras.HeaderText = "Tipo de producto";
            this.TipoProductoCodBarras.Name = "TipoProductoCodBarras";
            // 
            // MarcaPtoductoCodBarras
            // 
            this.MarcaPtoductoCodBarras.HeaderText = "Marca del producto";
            this.MarcaPtoductoCodBarras.Name = "MarcaPtoductoCodBarras";
            // 
            // PrecioCompraCodBarras
            // 
            this.PrecioCompraCodBarras.HeaderText = "Precio compra";
            this.PrecioCompraCodBarras.Name = "PrecioCompraCodBarras";
            // 
            // PrecioVnetaCodBarras
            // 
            this.PrecioVnetaCodBarras.HeaderText = "Precio Venta";
            this.PrecioVnetaCodBarras.Name = "PrecioVnetaCodBarras";
            // 
            // CantidadProductoCodBarras
            // 
            this.CantidadProductoCodBarras.HeaderText = "Cantidad producto";
            this.CantidadProductoCodBarras.Name = "CantidadProductoCodBarras";
            // 
            // CantidadMinProductoCodBarras
            // 
            this.CantidadMinProductoCodBarras.HeaderText = "Cantidad minima de productos";
            this.CantidadMinProductoCodBarras.Name = "CantidadMinProductoCodBarras";
            // 
            // UnidadMedidaCodBarras
            // 
            this.UnidadMedidaCodBarras.HeaderText = "Unidad de medida";
            this.UnidadMedidaCodBarras.Name = "UnidadMedidaCodBarras";
            // 
            // FechaCompraCodBarras
            // 
            this.FechaCompraCodBarras.HeaderText = "Fecha de compra";
            this.FechaCompraCodBarras.Name = "FechaCompraCodBarras";
            // 
            // FechaCaducidadCodBarras
            // 
            this.FechaCaducidadCodBarras.HeaderText = "Fecha de caducidad";
            this.FechaCaducidadCodBarras.Name = "FechaCaducidadCodBarras";
            // 
            // Busqueda
            // 
            this.Busqueda.Controls.Add(this.BusquedaDeProducto);
            this.Busqueda.Controls.Add(this.textBox2);
            this.Busqueda.Controls.Add(this.PorCodigoBarras);
            this.Busqueda.Controls.Add(this.BusquedaPormarca);
            this.Busqueda.Controls.Add(this.PorMarca);
            this.Busqueda.Controls.Add(this.textBox1);
            this.Busqueda.Controls.Add(this.PortipoProducto);
            this.Busqueda.Controls.Add(this.Busquedapornombre);
            this.Busqueda.Controls.Add(this.PorNombre);
            this.Busqueda.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Busqueda.Location = new System.Drawing.Point(345, 287);
            this.Busqueda.Name = "Busqueda";
            this.Busqueda.Size = new System.Drawing.Size(349, 211);
            this.Busqueda.TabIndex = 33;
            this.Busqueda.TabStop = false;
            this.Busqueda.Text = "Busqueda";
            this.Busqueda.Visible = false;
            // 
            // BusquedaDeProducto
            // 
            this.BusquedaDeProducto.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BusquedaDeProducto.Location = new System.Drawing.Point(137, 166);
            this.BusquedaDeProducto.Name = "BusquedaDeProducto";
            this.BusquedaDeProducto.Size = new System.Drawing.Size(92, 32);
            this.BusquedaDeProducto.TabIndex = 40;
            this.BusquedaDeProducto.Text = "Buscar";
            this.BusquedaDeProducto.UseVisualStyleBackColor = true;
            this.BusquedaDeProducto.Click += new System.EventHandler(this.BusquedaDeProducto_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(227, 121);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(116, 31);
            this.textBox2.TabIndex = 39;
            // 
            // PorCodigoBarras
            // 
            this.PorCodigoBarras.AutoSize = true;
            this.PorCodigoBarras.Location = new System.Drawing.Point(6, 123);
            this.PorCodigoBarras.Name = "PorCodigoBarras";
            this.PorCodigoBarras.Size = new System.Drawing.Size(231, 29);
            this.PorCodigoBarras.TabIndex = 38;
            this.PorCodigoBarras.Text = "Por codigo de barras";
            this.PorCodigoBarras.UseVisualStyleBackColor = true;
            // 
            // BusquedaPormarca
            // 
            this.BusquedaPormarca.Location = new System.Drawing.Point(127, 86);
            this.BusquedaPormarca.Name = "BusquedaPormarca";
            this.BusquedaPormarca.Size = new System.Drawing.Size(216, 31);
            this.BusquedaPormarca.TabIndex = 37;
            // 
            // PorMarca
            // 
            this.PorMarca.AutoSize = true;
            this.PorMarca.Location = new System.Drawing.Point(6, 85);
            this.PorMarca.Name = "PorMarca";
            this.PorMarca.Size = new System.Drawing.Size(129, 29);
            this.PorMarca.TabIndex = 36;
            this.PorMarca.Text = "Por marca";
            this.PorMarca.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(198, 52);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(145, 31);
            this.textBox1.TabIndex = 34;
            // 
            // PortipoProducto
            // 
            this.PortipoProducto.AutoSize = true;
            this.PortipoProducto.Location = new System.Drawing.Point(6, 54);
            this.PortipoProducto.Name = "PortipoProducto";
            this.PortipoProducto.Size = new System.Drawing.Size(193, 29);
            this.PortipoProducto.TabIndex = 35;
            this.PortipoProducto.Text = "Tipo de producto";
            this.PortipoProducto.UseVisualStyleBackColor = true;
            // 
            // Busquedapornombre
            // 
            this.Busquedapornombre.Location = new System.Drawing.Point(150, 19);
            this.Busquedapornombre.Name = "Busquedapornombre";
            this.Busquedapornombre.Size = new System.Drawing.Size(193, 31);
            this.Busquedapornombre.TabIndex = 34;
            this.Busquedapornombre.TextChanged += new System.EventHandler(this.Busquedapornombre_TextChanged);
            // 
            // PorNombre
            // 
            this.PorNombre.AutoSize = true;
            this.PorNombre.Location = new System.Drawing.Point(6, 21);
            this.PorNombre.Name = "PorNombre";
            this.PorNombre.Size = new System.Drawing.Size(142, 29);
            this.PorNombre.TabIndex = 34;
            this.PorNombre.Text = "Por nombre";
            this.PorNombre.UseVisualStyleBackColor = true;
            // 
            // AgregarVenta
            // 
            this.AgregarVenta.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AgregarVenta.Location = new System.Drawing.Point(671, 206);
            this.AgregarVenta.Name = "AgregarVenta";
            this.AgregarVenta.Size = new System.Drawing.Size(169, 38);
            this.AgregarVenta.TabIndex = 34;
            this.AgregarVenta.Text = "Agregar a venta";
            this.AgregarVenta.UseVisualStyleBackColor = true;
            this.AgregarVenta.Visible = false;
            // 
            // BorrardeVneta
            // 
            this.BorrardeVneta.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BorrardeVneta.Location = new System.Drawing.Point(669, 250);
            this.BorrardeVneta.Name = "BorrardeVneta";
            this.BorrardeVneta.Size = new System.Drawing.Size(169, 38);
            this.BorrardeVneta.TabIndex = 35;
            this.BorrardeVneta.Text = "Borrar de la venta";
            this.BorrardeVneta.UseVisualStyleBackColor = true;
            this.BorrardeVneta.Visible = false;
            // 
            // ModificarVenta
            // 
            this.ModificarVenta.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ModificarVenta.Location = new System.Drawing.Point(669, 291);
            this.ModificarVenta.Name = "ModificarVenta";
            this.ModificarVenta.Size = new System.Drawing.Size(192, 40);
            this.ModificarVenta.TabIndex = 36;
            this.ModificarVenta.Text = "Modificar producto";
            this.ModificarVenta.UseVisualStyleBackColor = true;
            this.ModificarVenta.Visible = false;
            // 
            // GuadarDatosConCaducidad
            // 
            this.GuadarDatosConCaducidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GuadarDatosConCaducidad.Location = new System.Drawing.Point(1068, 225);
            this.GuadarDatosConCaducidad.Name = "GuadarDatosConCaducidad";
            this.GuadarDatosConCaducidad.Size = new System.Drawing.Size(274, 56);
            this.GuadarDatosConCaducidad.TabIndex = 37;
            this.GuadarDatosConCaducidad.Text = "Guardar datos";
            this.GuadarDatosConCaducidad.UseVisualStyleBackColor = true;
            this.GuadarDatosConCaducidad.Visible = false;
            this.GuadarDatosConCaducidad.Click += new System.EventHandler(this.GuadarDatosConCaducidad_Click);
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1354, 498);
            this.Controls.Add(this.GuadarDatosConCaducidad);
            this.Controls.Add(this.ModificarVenta);
            this.Controls.Add(this.BorrardeVneta);
            this.Controls.Add(this.AgregarVenta);
            this.Controls.Add(this.Busqueda);
            this.Controls.Add(this.dataGridView1conCaducidad);
            this.Controls.Add(this.GroupBoxManejaProductosPereceders);
            this.Controls.Add(this.BotonAgregarProducto);
            this.Controls.Add(this.BotonDardeBajaProducto);
            this.Controls.Add(this.BotonGuardarCambios);
            this.Controls.Add(this.groupBoxDarDeBajaEsteProducto);
            this.Controls.Add(this.dataGridView2Modificardatos);
            this.Controls.Add(this.BotonFinalizarventaConFactura);
            this.Controls.Add(this.BotonFinalizarVentaConNota);
            this.Controls.Add(this.BotonFinalizarVentaSimple);
            this.Controls.Add(this.dataGridView2ParalaVenta);
            this.Controls.Add(this.dataGridView2Ventassimples);
            this.Controls.Add(this.EtiquetadeAdbertenciadeSeleccion);
            this.Controls.Add(this.BotonGuardarDatosGenerales);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBoxTienesAdeudos);
            this.Controls.Add(this.checkboxTieneTrabajadores);
            this.Controls.Add(this.Facturaono);
            this.Controls.Add(this.BotonGuardardatostrabajadoresclientes);
            this.Controls.Add(this.dataGridView2TrabajadorasClientes);
            this.Controls.Add(this.BotonGuardarProductos);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(menuStrip2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimizeBox = false;
            this.Name = "Menu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menú";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Menu_Load);
            menuStrip2.ResumeLayout(false);
            menuStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2TrabajadorasClientes)).EndInit();
            this.Facturaono.ResumeLayout(false);
            this.Facturaono.PerformLayout();
            this.checkboxTieneTrabajadores.ResumeLayout(false);
            this.checkboxTieneTrabajadores.PerformLayout();
            this.groupBoxTienesAdeudos.ResumeLayout(false);
            this.groupBoxTienesAdeudos.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2Ventassimples)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2ParalaVenta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2Modificardatos)).EndInit();
            this.groupBoxDarDeBajaEsteProducto.ResumeLayout(false);
            this.groupBoxDarDeBajaEsteProducto.PerformLayout();
            this.GroupBoxManejaProductosPereceders.ResumeLayout(false);
            this.GroupBoxManejaProductosPereceders.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1conCaducidad)).EndInit();
            this.Busqueda.ResumeLayout(false);
            this.Busqueda.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStripMenuItem comprasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificarProductoExistenteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem darDeBajaUnProductoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agregarNuevoProductoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ventasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem llenadoDeDatosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contabilidadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ventaSimpleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ventaConNotaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ventaConFacturaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem clientesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem empresaToolStripMenuItem;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button BotonGuardarProductos;
        private System.Windows.Forms.DataGridView dataGridView2TrabajadorasClientes;
        private System.Windows.Forms.Button BotonGuardardatostrabajadoresclientes;
        private System.Windows.Forms.GroupBox Facturaono;
        private System.Windows.Forms.CheckBox FacturaSi;
        private System.Windows.Forms.ToolStripMenuItem datosBasicosToolStripMenuItem;
        private System.Windows.Forms.GroupBox checkboxTieneTrabajadores;
        private System.Windows.Forms.CheckBox TienetrabajadoresCheckbox;
        private System.Windows.Forms.GroupBox groupBoxTienesAdeudos;
        private System.Windows.Forms.CheckBox TienesAdeudosCheckBox;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button BotonGuardarDatosGenerales;
        private System.Windows.Forms.Label EtiquetadeAdbertenciadeSeleccion;
        private System.Windows.Forms.DataGridView dataGridView2Ventassimples;
        private System.Windows.Forms.DataGridView dataGridView2ParalaVenta;
        private System.Windows.Forms.Button BotonFinalizarVentaSimple;
        private System.Windows.Forms.Button BotonFinalizarVentaConNota;
        private System.Windows.Forms.Button BotonFinalizarventaConFactura;
        private System.Windows.Forms.DataGridView dataGridView2Modificardatos;
        private System.Windows.Forms.GroupBox groupBoxDarDeBajaEsteProducto;
        private System.Windows.Forms.CheckBox checkBoxdardeBajaEsteProducto;
        private System.Windows.Forms.Button BotonGuardarCambios;
        private System.Windows.Forms.Button BotonDardeBajaProducto;
        private System.Windows.Forms.Button BotonAgregarProducto;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.GroupBox GroupBoxManejaProductosPereceders;
        private System.Windows.Forms.ToolStripMenuItem adeudosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gastosContinuosToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn CodigoBarras;
        private System.Windows.Forms.DataGridViewTextBoxColumn NombreGenerico;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoProducto;
        private System.Windows.Forms.DataGridViewTextBoxColumn MarcaProduco;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrecioCompra;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrecioVenta;
        private System.Windows.Forms.DataGridViewTextBoxColumn CantidadProducto;
        private System.Windows.Forms.DataGridView dataGridView1conCaducidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn CantidadMinProducto;
        private System.Windows.Forms.DataGridViewTextBoxColumn UnidadMedida;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaCompra;
        private System.Windows.Forms.DataGridViewTextBoxColumn RFCPersona;
        private System.Windows.Forms.DataGridViewTextBoxColumn ApehidoPaterno;
        private System.Windows.Forms.DataGridViewTextBoxColumn ApehidoMaterno;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumeroTelef;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumCelular;
        private System.Windows.Forms.DataGridViewTextBoxColumn DireccionFiscal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.DataGridViewCheckBoxColumn TabajadorCliente;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Registrados;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sueldo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ISRaPagar;
        private System.Windows.Forms.DataGridViewTextBoxColumn CodigodeBarras;
        private System.Windows.Forms.DataGridViewTextBoxColumn NombreGenericos;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoProductos;
        private System.Windows.Forms.DataGridViewTextBoxColumn MarcaProductos;
        private System.Windows.Forms.DataGridViewTextBoxColumn PreciodVenta;
        private System.Windows.Forms.DataGridViewTextBoxColumn CantidadProductos;
        private System.Windows.Forms.DataGridViewTextBoxColumn CodigoBarrasVenderSimple;
        private System.Windows.Forms.DataGridViewTextBoxColumn NombreGenericoaVender;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoProductoaVender;
        private System.Windows.Forms.DataGridViewTextBoxColumn MarcaProductoVender;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrecioVentaVendido;
        private System.Windows.Forms.DataGridViewTextBoxColumn CantidadProductoVenta;
        private System.Windows.Forms.DataGridViewTextBoxColumn CodBarrasCaducidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn NombreGenericoCodBarras;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoProductoCodBarras;
        private System.Windows.Forms.DataGridViewTextBoxColumn MarcaPtoductoCodBarras;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrecioCompraCodBarras;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrecioVnetaCodBarras;
        private System.Windows.Forms.DataGridViewTextBoxColumn CantidadProductoCodBarras;
        private System.Windows.Forms.DataGridViewTextBoxColumn CantidadMinProductoCodBarras;
        private System.Windows.Forms.DataGridViewTextBoxColumn UnidadMedidaCodBarras;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaCompraCodBarras;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaCaducidadCodBarras;
        private System.Windows.Forms.GroupBox Busqueda;
        private System.Windows.Forms.Button BusquedaDeProducto;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.CheckBox PorCodigoBarras;
        private System.Windows.Forms.TextBox BusquedaPormarca;
        private System.Windows.Forms.CheckBox PorMarca;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.CheckBox PortipoProducto;
        private System.Windows.Forms.TextBox Busquedapornombre;
        private System.Windows.Forms.CheckBox PorNombre;
        private System.Windows.Forms.Button AgregarVenta;
        private System.Windows.Forms.Button BorrardeVneta;
        private System.Windows.Forms.Button ModificarVenta;
        private System.Windows.Forms.Button GuadarDatosConCaducidad;
    }
}

