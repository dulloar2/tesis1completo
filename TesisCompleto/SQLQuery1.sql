create database Tesis1LoquenecesitaelUsuario1           /*creacion de la base de datos*/
use Tesis1LoquenecesitaelUsuario                       /*Poner en uso la base de datos creada en el renglon anterior*/

create table Producto                                 /*creacion de la primera tabla llamada Productos*/
(
IDProducto int identity primary key,                   /*LLave primaria tipo entero y es un identificador, no se colocaara ya que esta llave es unica y autoincrementable*/
CodigoBarras int not null,                             /*Codigo de barras no obligatorio, se busca que este se coloque con un lector de codigo de barras o de manera manual*/
NombreGenerico varchar(30) not null,                   /*Nombre generico de los productos (referido a que las tienda, papelerias, tlapalerias, cremerias, tienen muchos productos con el mimso nombre) por ejemplo Plumas*/
TipoProducto varchar(30) not null,                     /*Aqui puede drecribir el producto, siguiendo el ejemplo anterior aqui iria "Bic punto mediano azul" */
MarcaProducto varchar(20) not null,                    /*Marca del producto, siguiendo el ejemolo seria "Bic"*/
PrecioCompra float not null,                           /*Precio en el cual compraron el producto*/
PrecoVenta float not null,                             /*Precio en el cual pienza vender el producto*/
CantidadProducto float not null,                       /*Cantidad de producto que tiene en existencia*/
CantidadMInProducto float not null,                    /*Cantidad de producto minimo que requiere antes de comprar mas*/
UnidaddemedidaProductos varchar(10) not null,          /*se mide en kiligramos o por pieza*/
FechaCompra date not null,                             /*fecha en la que se dio de alta el producto*/
fechaCaducidad date,                                   /*si el producto es perecedero se introducira por medio de un calendario en el software*/
DardeBaja bit,                                         /*suspender producto*/
)

create table Controlar
(
id int identity primary key,                           /*identificado que es inutil*/
CaducidadProducto bit not null,                        /*El producto es perecedero si o no, en caso de que si el software generara el calendario */
Facturacion bit not null,                              /*si este esta en cero y se le da guardar los datos de esta tabla se autollenaran con valores falsos y los de clientes(guadados en la tabla de Personas tambien) y des habilitando las demas obciones*/
TienesTrabajadores bit not null,                        /*si no tiene trabajadores se auto compltaran los daosta de un solo sujeto y se bloqueara la obcbio*/ 
SinAdeudos bit not null,                               /*seleccion por si la empresa ya esta establecida y no tiene deudas*/
TienenAdedosAusted bit not null,
)
                     /*La siguientes tablas son creadas por si la PyME desea hacer facturacion, enc aso de que no solo se inabilitara esta funcion y quedaran basia*/

create table DatosPyME                                /*Tabla 2 Datos de la aempresa a en la cual se implementara el software(solo en caso de que quiera o realize facturacion)*/
(
NumLocal int identity primary key,                     /*solo para identificar ya que es necesario*/
RFCPropietario varchar(15),                            /*Registro federal de contibuyentes del propietario*/
ApehidoPeternoPropietario varchar(20) not null,        /*Apehido paterno del propietario*/
ApehidoMaternoPrpietario varchar(20) not null,         /*Apehido materno del propietario*/
NombrePropietario varchar(20) not null,                /*Nombre del propietario*/
NombreLocal varchar(50) not null,                      /*Nombre del estableciomiento  o micro pequela o mediana empresa*/
NumTelefonicoPrpietarioLocal int,                      /*Numero de contacto con el local*/
DireccionFiscal varchar(50) not null,                  /*Direccion fical en donde esta localizada la micro, peque�a o mediana empresa*/
emailempresa  varchar(50) not null,                    /*Para poder hace contacto con la sempresa*/  
GananciaEsperada float not null,                       /*es el sueldo esprerado por la persona due�a de la PyME y que es la unica persona que traja ahi*/
)

create table DatosPersonas                              /*Aqui registramos a las personas que tienen algo que ver con la empresa como clientes a los que se les factura ya a los trabajadores*/
(
IDPersona int identity primary key,                    /*identificador de cliente o trabajador*/
RFCCliente varchar(15) not null,                       /*Registro federal de contibuyentes del cliente*/
ApehidoPeternoCliente varchar(20) not null,            /*Apehido paterno del cliente*/
ApehidoMaternoCliente varchar(20) not null,            /*Apehido materno del cliente*/
NombreCliente varchar(20) not null,                    /*Nombre del cliente*/
NumTelefonicoCiente int,                               /*Numero de contacto dl cliente*/
Numtelcelular int,                                     /*Numero de contacto dl cliente personal*/
DireccionFiscal varchar(50) not null,                  /*Direccion fical del contribuyente*/
email varchar(50) not null,                            /*Correo electronico del cliente*/
Trabajadorocliente bit not null,                       /*solo para separar*/
Registrados bit not null,                              /*si no los tienen registrados se aoutollenara el rfc con 0, direccion fical */
sueldo float not null,                                 /*Aunque no este registrado esta persona percibe un sueldo*/
ISR float not null,                                    /*Si la persona trabajador de la PyME se autocalcula el Isr del 10%*/
)

                                    /*Tablas creadas para la parte contable del sitema*/

create table Adeudo                                   /*Para las empresas nacientes y no tan nacientes es bueno considerar lo egresos que esto genera*/
(
IDAdeudo int identity primary key,                     /*identificado de adeudo*/
NombreEmpesaAdeudo varchar(50) not null,               /*nombre de la empresa a la que se le adeuda*/
MontoTotalAdeudo float not null,                       /*Cantidad de adeudo adquirido*/
MontoPagado float not null,                            /*Moto pagada desde que se adquirio la deuda hasta el momento de adquirir este sistema*/
CatidadDineroPagar float not null,                     /*Monto que paga cada determinado tiempo*/
PlazoporPago varchar not null,                         /*Plazo entre pagos*/
FechaPago date not null,                               /*Fechapara hacer el pago*/
) 

create table Gastosfrecuentes                         /*todos aquellos gastos que la micro, peque�a o mediana empresa */
(
IDGastosContinuos int identity primary key,            /*Cantidad de gastos*/
Agua float not null,                                   /*gasto por uso de agua*/
EnergiaEelctrica float not null,                       /*gasto por uso de energia electrica*/
Telefono float,                                        /*gasto por uso de Telefono*/
internet float,                                        /*gasto por uso de internet*/
RentaMoviliario float not null,                        /*gasto por uso de renta de miviliario*/
RentaLocal float not null,                             /*gasto por uso de renta de un establecimiento*/
Mantenimiento float not null,                          /*gasto por uso de mantenimiento*/
)

create table GastosApagar
(
id int identity primary key,
PrecioVentaf float not null,
PrecioVentav float not null, 
Agua float not null,                                   /*gasto por uso de agua*/
EnergiaEelctrica float not null,                       /*gasto por uso de energia electrica*/
Telefono float not null,                               /*gasto por uso de Telefono*/
internet float not null,                               /*gasto por uso de internet*/
RentaMoviliario float not null,                        /*gasto por uso de renta de miviliario*/
RentaLocal float not null,                             /*gasto por uso de renta de un establecimiento*/
Mantenimiento float not null,                          /*gasto por uso de mantenimiento*/
NombreEmpesaAdeudo varchar(50) not null,               /*nombre de la empresa a la que se le adeuda*/
MontoTotalAdeudo float not null,                       /*Cantidad de adeudo adquirido*/
MontoPagado float not null,                            /*Moto pagada desde que se adquirio la deuda hasta el momento de adquirir este sistema*/
CatidadDineroPagar float not null,                     /*Monto que paga cada determinado tiempo*/
FechaPago date not null,                               /*Fechapara hacer el pago*/
)
                                    /*Consultas realizadas para solb�ventar problemas*/
                /*esta tabla depende 100% de la relacion de la tabla produco con la ventana de venta, esues de ellos migrara a informacion a esta tabla*/

create table Vendidonota
(
IDVentastotales int identity primary key,
IDVenta int not null,                           /*identificador de la venta*/
IDProducto int not null,                        /*copia del ID del producto*/
NombreProducto varchar(30) not null,            /*copia del nombre del producto*/
MarcaProducto varchar(20) not null,             /*copia del marca del producto*/
PrecioVenta float not null,                     /*copia del precio de venta del producto*/
CantidadProducto float not null,                /*copia del cantidad de producto vendido*/
IDFactura int not null,
CodigoBarras int not null,
NombreProductof varchar(30) not null,
PrecioVentaf float not null,
NumeroProductos float not null,
)

create table Ventas
(
IDVenta int identity primary key,               /*identificador de la venta*/
IDProducto int not null,                        /*copia del ID del producto*/
NombreProducto varchar(30) not null,            /*copia del nombre del producto*/
MarcaProducto varchar(20) not null,             /*copia del marca del producto*/
PrecioVenta float not null,                     /*copia del precio de venta del producto*/
CantidadProducto float not null,                /*copia del cantidad de producto vendido*/
FechaVenta date not null,                       /*copia del fecha de venta del producto*/
)
create table Facturas 
(
IDFactura int identity primary key,
CodigoBarras int not null,
NombreProducto varchar(30) not null,
PrecioVenta float not null,
NumeroProductos float not null,
NombreLocal varchar(50) not null,                            /*DatosLocal*/
RFCLocal varchar(15) not null,
DireccionFiscal varchar(50) not null,
NumeroTelefonicoPropietario int not null,
EmailLocal varchar(50) not null,
RFCCliente varchar(15) not null,                            /*DatosCliente*/
ApehidoPaternoCliente varchar(20) not null,
ApehidoMaternoCliente varchar(20) not null,
NomblreCliente varchar(20) not null,
FechaVenta date not null,                              /*fecha en la que se vendio el producto*/
)
create table Estadisticas
(
IDEstadistica int identity primary key,
fechaCorte date not null,
fechaIncio date not null,
NombreProducto varchar(30) not null, 
MarcaProducto varchar(20) not null,
PrecioVenta float not null,
CantidadProducto float not null,
cantidadincialProducto float not null 
)